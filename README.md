<h1> <img src="https://fonts.gstatic.com/s/e/notoemoji/latest/1f3af/512.gif" alt="🎯" width="28" height="28"> Workshop : E-Commerce</h1>

Workshop,Learning with React + Tailwind CSS, Node.js, MongoDB to make a E-Commerce Web Application

<br>

<h1> <img src="https://fonts.gstatic.com/s/e/notoemoji/latest/1f44b_1f3fb/512.gif" alt="👋" width="28" height="28"> My Team</h1>

- <a href="https://github.com/BossBoxing">Boss (Hudsawat Akkati)</a>
    - Software Developer @ Friend Robot
- <a href="https://github.com/BillyForce43">Bill (Nattawat Saetung)</a>
    - Programmer @ INET Khon Kaen
- <a href="https://github.com/TheerapatL">Beer (Theerapat Loinok)</a>
    - Front-End Developer @ Woxa Corporation

<br>

<h1>
  <img src="https://fonts.gstatic.com/s/e/notoemoji/latest/23f0/512.gif" alt="⏰" width="28" height="28"> Board of Project</h1>

- <a href="https://wooded-modem-014.notion.site/Web-Workshop-1-E-Commerce-c4cf159699d04e16ba66f0375571c49d?pvs=4">Nothion</a>